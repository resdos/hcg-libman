import { Component, OnInit } from '@angular/core';
import { DataService } from '../common/services/data.service';
import { ToolsetService } from '../common/services/toolset.service';
import { UsersDataService } from './users-data.service';
import { ModalController } from '@ionic/angular';
import { UserFormComponent } from './user-form/user-form.component';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

  constructor(
    public data: DataService,
    public user_data: UsersDataService,
    public _ts: ToolsetService, 
    public _auth: AuthService, 
    public modal: ModalController,
  ) {

    this.user_data.user_list.subscribe(data => {

      this.users = data
      this.is_loading = false
      this.filter_users()
      console.log('USER LIST UPDATED : ', this.users);

    })


    this._auth._user.subscribe(user_doc => {

      this.meta.current_user = user_doc

    })

    this._auth.update_user_data()


    
  }

  meta: any = {
    search: '',
    current_user: {},
  }

  ngOnInit() {
  }


  is_loading = true
  users
  users_list
 

 
  ionViewWillEnter() {

    this.load_data()
 
  }



  filter_users() {
    this.users_list = this.users.filter(u => {
      return (
        String(u.name).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, '').includes(String(this.meta.search).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, ''))
        || String(u.role).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, '').includes(String(this.meta.search).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, ''))
        || String(u.username).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, '').includes(String(this.meta.search).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, ''))
        || String(u._id).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, '').includes(String(this.meta.search).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, ''))
      )
    })
  }

  load_data() {
    this.is_loading = true
    return this.user_data.fetch_users().finally(() => {
      this.is_loading = false
    })

  }


  show_user_info(user) {

    this._ts.show_alert({
      title: 'User Profile',
      icon: 'info',
      html: `
        <ul class="list-group text-left">
          <li class="list-group-item list-group-item-action">
            Name: <span class="float-right text-primary font-weight-bold"> ${user['name'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            User ID: <span class="float-right"> ${user['_id'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Username: <span class="float-right"> ${user['username'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Email ID: <span class="float-right"> ${user['email'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Phone : <span class="float-right"> ${user['phone'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Books in Hand : <span class="float-right position-absolute px-3 py-1 rounded text-light bg-danger font-weight-bold" style="font-size: 16px;right: 1rem;top: 0.45rem;"> 5 </span>
          </li>
          
          <li class="list-group-item list-group-item-action">
            Role : <b class="float-right"> ${user['role'] || ''} </b>
          </li>
          <li class="list-group-item list-group-item-action ">
            Permissions : <br> ${ user['permissions'].map(i=> '<span class="p-1 px-2 mx-1 badge medium font-weight-normal">' + i +'</span>') || ''}  
          </li>
        </ul>
      `,
      showConfirmButton: true
    })

  }





  delete_user(user) {

    if (user && user.reserved) {
      return this._ts.show_alert({
        title: 'Sorry!',
        icon: 'error',
        html: ` 
          <br>
          <b class="text-danger"> " ${user['name'] || ''} " </b>  
          <br> 
          <br> 
          is a <b class="text-success">Reserved User</b>
          <br>You cannot delete a reserved user!
          <br>
          -
        `,
        timer: 4000,
        showConfirmButton: false,
      }).then(result => {
        return 
      })


    }

    this._ts.show_alert({
      title: 'Delete User ?',
      icon: 'warning',
      html: `
        <br>
        You're about to delete user <br> <b class="text-danger"> " ${user['name'] || ''} " </b>  <br> Are you sure? 
        <br>
        <br>

        <ul class="list-group text-left">
          <li class="list-group-item list-group-item-action">
            Name: <span class="float-right"> ${user['name'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            User ID: <span class="float-right"> ${user['_id'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Username: <span class="float-right"> ${user['username'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Email ID: <span class="float-right"> ${user['email'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Phone : <span class="float-right"> ${user['phone'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Books in Hand : <span class="float-right position-absolute px-3 py-1 rounded text-light bg-danger font-weight-bold" style="font-size: 16px;right: 1rem;top: 0.45rem;"> 5 </span>
          </li>

          <li class="list-group-item list-group-item-action">
            Role : <b class="float-right"> ${user['role'] || ''} </b>
          </li>
          <li class="list-group-item list-group-item-action ">
            Permissions : <br> ${ user['permissions'].map(i => '<span class="p-1 px-2 mx-1 badge medium font-weight-normal">' + i + '</span>') || ''}  
          </li>
        </ul>
      `,
      showConfirmButton: true,
      confirmButtonText: 'Yes',
      showCancelButton: true,
    }).then(result => {
      if (result.value) {

        this.user_data.delete_user(user).then(data => {
          
          this.user_data.fetch_users()
          
          this._ts.show_alert({
            title: 'Deleted!',
            html: `<br> User <b class="text-danger">" ${user['name'] || ''} "</b> <br> with User ID: <b> ${user['_id'] || ''} </b> <br> has been deleted successfully!`,
            icon: 'success',
            showConfirmButton: false,
            timer: 2500
          })

        })

      }
    })

  }


  open_user_form(mode = 'ADD', user = {}) {

    console.log('Opening ' + mode + ' User: ', user);

    this.modal.create({
      component: UserFormComponent,
      backdropDismiss: false,
      componentProps: {
        user: user,
        meta: { title: mode + ' User', mode }
        // lastName: 'Adams',
        // middleInitial: 'N'
      }
    }).then(modal => {
      modal.present()
    })


  }

}
