import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToolsetService } from 'src/app/common/services/toolset.service';
import { UsersDataService } from '../users-data.service';
import { RolesPermissionsDataService } from 'src/app/roles-permissions/roles-permissions-data.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {

  constructor(
    public modal: ModalController,
    public _ts: ToolsetService,
    public user_data: UsersDataService,
    public rp_data: RolesPermissionsDataService,

  ) {

  }

  @Input() user
  @Input() meta = {
    mode: 'ADD',
    title: 'ADD User',
    req_reset_pwd: false,
    role_list: []
  }

  form: any = {
    _id: '',
    type: "USER",
    role: "READER",
    user_index: "",
    permissions: [],
    username: "",
    password: "123",
    first_name: "",
    last_name: "",
    email: "",
    phone: "",
    name: ''
  }

  
  ngOnInit() {

    this.form = { ...this.form, ...this.user }
 
    this.meta.role_list = this.rp_data.roles_permissions.value['roles']

  }
 


  close_modal() { 
    this.modal.dismiss({
      'dismissed': true
    });
  }


  formatInput(text) {
    if (text) {
      return String(text+'').toLowerCase().replace('-', '_').replace(/[^\w\s]/gi, '') || ''
    }
    return ''
  }



  save_user() {

    if (this.meta.req_reset_pwd || this.meta.mode == 'ADD') {
      this.form.password = this._ts.hash(this.form.password)
    }
    const { name, user_index, permissions, has_permission, ...new_doc } = this.form 

    this.user_data.save_user({...new_doc, mode: this.meta.mode}).then(data => {

      this.user_data.fetch_users()

      this._ts.show_alert({
        title: (this.meta.mode == 'ADD') ? 'User Created!' : 'User Updated!',
        icon: 'success',
        html: `User profile <br> <b class="text-info"> ${new_doc.username}</b> <br>has been ${ (this.meta.mode == 'ADD') ? 'created' : 'updated' } successfully!`,
        showConfirmButton: false, 
        timer: 2000,
      })
      this.close_modal()
    })

  }


}
