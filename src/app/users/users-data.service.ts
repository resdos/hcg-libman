import { Injectable } from '@angular/core';
import { DataService } from '../common/services/data.service';
import { ToolsetService } from '../common/services/toolset.service';
import { BehaviorSubject } from 'rxjs';
import { resolve } from 'url';
import { RolesPermissionsDataService } from '../roles-permissions/roles-permissions-data.service';

@Injectable({
  providedIn: 'root'
})
export class UsersDataService {


  constructor(
    public db: DataService,
    public rp_data: RolesPermissionsDataService,
    public _ts: ToolsetService
  ) {
    this.fetch_users()
  }

  is_loading = new BehaviorSubject(true)





  user_list = new BehaviorSubject([])

  fetch_users() {

    return new Promise((resolve, reject) => {

      this.db.find({
        selector: {
          type: "USER",
        },
        use_index: "type_index"
      }).then(result => {

        const is_valid = result && result.docs && result.docs.length
        // handle doc
        console.log('USER LIST fetched!', result);

        if (is_valid) {
          const user_list = result.docs.map(u => {
            // const { password, ...user_data } = u
            const { ...user_data } = u

            const permissions = [...this.rp_data.roles_permissions.value['role_permissions'][u['role']]]

            const has_permission = {}

            if (permissions && permissions.length) {

              for (let key of permissions) {
                has_permission[key] = true;
              }

            }

            return {
              ...user_data,
              name: `${u.first_name} ${u.last_name}`,
              permissions,
              has_permission,
              my_books: (user_data['my_books']) ? { ...user_data['my_books'] } : {} 
            }
          })

          
          this.user_list.next([...user_list])
        }
        resolve(result)

      }).catch( (err) => {

        console.log(err);
        // this._ts.show_alert({
        //   title: 'Network Error',
        //   icon: 'error',
        //   html: `Can't connect to Server!`,
        //   timer: 2000
        // })

        reject(err)

      })

    })

  }





  save_user(new_data) {

    return new Promise((resolve, reject) => {


      let { _rev, mode, _id, ...new_doc } = new_data

      // ADD MODE
      if (!new_data['_id']) {

        this.db.find({
          selector: {
            type: "USER",
          },
          sort: [{ _id: "desc" }],
          limit: 1,
          fields: ["user_index", "_id"]
        }).then(result => {

          const is_valid = result && result.docs && result.docs.length

          // GOT LAST USER INDEX
          let next_user_index
          if (is_valid) {
            next_user_index = String((+result['docs'][0]['user_index'] + 1)).padStart(5, '0')
            
          } else {
            next_user_index = String((0+ 1)).padStart(5, '0')

          }

            // console.log('CHECKUP ', this.rp_data.roles_permissions.value);

            const post_data = {
              ...new_doc,
              _id: new_doc['username'] + '-' + next_user_index,
              user_index: next_user_index,
              // permissions: [...this.rp_data.roles_permissions.value['role_permissions'][new_doc['role']]]
            }

            console.log('post_data: ', post_data);

            this.db.put(post_data).then( (result) => {

              this.fetch_users()
              resolve(result)

            }).catch( (err) => {

              console.log(err);
              reject(err)

            })


        }).catch(err => {
 

            const next_user_index = String((0 + 1)).padStart(5, '0')
  
            console.log('CHECKUP ', this.rp_data.roles_permissions.value);
  
            const post_data = {
              ...new_doc,
              _id: new_doc['username'] + '-' + next_user_index,
              user_index: next_user_index,
              // permissions: [...this.rp_data.roles_permissions.value['role_permissions'][new_doc['role']]]
            }
  
            console.log('post_data: ', post_data);
  
            this.db.put(post_data).then( (result) => {
  
              this.fetch_users()
              resolve(result)
  
            }).catch( (err) => {
  
              console.log(err);
              reject(err)
  
            })
 


        })


      }

      // EDIT MODE
      else {

        this.db.get(new_data['_id']).then(result => {


          if (new_data['_id'] == result['_id'] && new_data['_rev'] == result['_rev']) {

            // console.log('CONFIRMED USER: ', result);

            new_doc = {
              ...result, ...new_doc,
              // permissions: [...this.rp_data.roles_permissions.value['role_permissions'][new_doc['role']]]
            }

            console.log('CONFIRM USER UPDATED: ', new_doc);

            this.db.put({ ...new_doc }).then( (result) => {

              this.fetch_users()
              resolve(result)

            }).catch( (err) => {

              console.log(err);
              reject(err)

            })


          }


        })

      }


    })

  }


  delete_user(user_doc) {

    return new Promise((resolve, reject) => {

      this.db.remove(user_doc).then(data => {
        this.fetch_users()
        resolve(data)
      }).catch(err => {
        reject(err)
      })

    })
  }

}
