import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, from } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { AuthResponse } from './auth.model';
import { Plugins } from '@capacitor/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../common/services/data.service';
import { UsersDataService } from '../users/users-data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {



  constructor(
    private _data: DataService,
    private user_data: UsersDataService,

  ) {

  }

  public _user = new BehaviorSubject(null);
  private _isAuthenticated: boolean = false;


  get isAuthenticated(): Observable<boolean> {
    return of(this._isAuthenticated);
  }

  autoLogin(): Observable<boolean> {
    return from(Plugins.Storage.get({ key: 'user' })).pipe(
      map(storedData => {
        if (!storedData || !storedData['value']) {
          return false;
        }
        this.update_user_data()
        return true;
      })
    );
  }

  login(username: string, password: string): Observable<AuthResponse> {

    return from(this.login_user(username, password))

  }




  update_user_data() {

    return new Promise((resolve, reject) => {

      console.log('AUTH HAS USER : ', this._user.value);
      const cur_user = JSON.parse(localStorage.getItem('_cap_user') || '{}')

      if (cur_user && cur_user['user'] && cur_user['user']['_id']) {

        this._data.get(cur_user.user._id).then(user_doc => {

          if (user_doc && user_doc['_id']) {
            const { password, ...user } = user_doc
            user.name = user.first_name + " " + user.last_name

            this._data.get('ROLES_PERMISSIONS').then(result => {

              user.permissions = result.role_permissions[user.role]
              user.has_permission = {}

              if (user && user.permissions && user.permissions.length) {

                for (let key of user.permissions) {
                  user.has_permission[key] = true;
                }

              }


              const local_user = JSON.parse(localStorage.getItem('_cap_user'))
              local_user.user = user

              Plugins.Storage.set({ key: 'user', value: JSON.stringify(local_user) });

              this._user.next(user)

              resolve(user)

            }).catch(err => reject(err))

          } else {

            reject(false)

          }

        })
      } else {

        this._user.next(null)
        reject(false)
      }

    })

  }

  login_user(username, password): Promise<AuthResponse> {
    return new Promise((resolve, reject) => {

      setTimeout(() => {

        this._data.login_user(username, password).then(payload => {

          if (payload['is_authenticated'] === true) {
            this._isAuthenticated = true;
            Plugins.Storage.set({ key: 'user', value: JSON.stringify(payload) });
            console.log('LOGIN SUCCESS!');

            this.user_data.fetch_users()

            this._user.next(payload['user'] || null)
            resolve({ isAuthenticated: true });
          } else {
            this._isAuthenticated = false;

            console.log('LOGIN FAILED! ', payload['msg']);

            resolve({
              isAuthenticated: false,
              errorMessage: payload['msg']
            });
          }

        }).catch(err => {

          console.log('LOGIN FAILED!', err);

          resolve({
            isAuthenticated: false,
            errorMessage: err['msg']
          });
        })

      }, 1000);

    })
  }


  logout() {
    Plugins.Storage.remove({ key: 'user' });
    this._isAuthenticated = false;
    this._user.next(null)

    return this._isAuthenticated;
  }

  check_auth() {
    return Plugins.Storage.get({ key: 'user' })
  }

}
