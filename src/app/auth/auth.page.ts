import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoadingController, Platform, NavController, IonInput } from '@ionic/angular';
import { ToolsetService } from '../common/services/toolset.service';
import { DataService } from '../common/services/data.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss']
})
export class AuthPage implements OnInit, OnDestroy, AfterViewInit {
  backButtonSubscription;
  errorMessage = '';

  // usernameCtrl
  // passwordCtrl
  @ViewChild('test', { static: false }) usernamefield: IonInput;

  constructor(
    private platform: Platform,
    private authService: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private _ts: ToolsetService,
    private db: DataService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    // this.db
  }

  ionViewWillEnter() {
    this.errorMessage = '';

    console.log('HASH : ', this._ts.hash(123));

  }


  login_test() {
    this.authService.login('ahmed', '123')
  }

  onLogin(form: NgForm) {

    this._ts.show_loader({
      message: 'Logging in..',
    }).then(loadEl => {

      
        this.authService
          .login(form.value.username, form.value.password)
          .subscribe(resp => {
            if (resp.isAuthenticated) {

              this.router.navigateByUrl('home');
              this._ts.hide_loader();
              this.errorMessage = '';

            } else {

              this.errorMessage = resp.errorMessage;
              this._ts.hide_loader();

            }
            form.reset();
          }, err => {
            this._ts.hide_loader();
          });

      });
  }


  ngAfterViewInit(): void {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      if (
        window.location.pathname === '/home' ||
        window.location.pathname === '/login'
      ) {
        navigator['app'].exitApp();
        window.location.pathname = '/login'
      } else {
        this.navCtrl.back();
      }
    });

  }

  ngOnDestroy(): void {
    this.backButtonSubscription.unsubscribe();
  }
}
