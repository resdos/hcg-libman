export interface AuthResponse {
  isAuthenticated: boolean;
  token?: string;
  errorMessage?: string;
}
