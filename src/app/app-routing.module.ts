import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
   
  

  {
    path: 'login',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthPageModule),
  },


  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
    canLoad: [AuthGuard],

  },

  // {
  //   path: 'list',
  //   loadChildren: () => import('./list/list.module').then(m => m.ListPageModule),
  //   canLoad: [AuthGuard],
  // },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule),
    canLoad: [AuthGuard],

  },
  {
    path: 'users',
    loadChildren: () => import('./users/users.module').then( m => m.UsersPageModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'books',
    loadChildren: () => import('./books/books.module').then( m => m.BooksPageModule),
  },
  {
    path: 'roles-permissions',
    loadChildren: () => import('./roles-permissions/roles-permissions.module').then( m => m.RolesPermissionsPageModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'my-books',
    loadChildren: () => import('./my-books/my-books.module').then(m => m.MyBooksPageModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'book-search',
    loadChildren: () => import('./book-search/book-search.module').then( m => m.BookSearchPageModule)
  },

  { path: '**', redirectTo: '/home' },  {
    path: 'shared',
    loadChildren: () => import('./common/modules/shared/shared.module').then( m => m.SharedPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
