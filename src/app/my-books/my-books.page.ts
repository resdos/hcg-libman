import { Component, OnInit } from '@angular/core';
import { DataService } from '../common/services/data.service';
import { AuthService } from '../auth/auth.service';
import { BookDataService } from '../books/book-data.service';
import { ToolsetService } from '../common/services/toolset.service';
import { ModalController } from '@ionic/angular';
import { UsersDataService } from '../users/users-data.service';
import { BookCommentComponent } from '../books/book-comment/book-comment.component';

@Component({
  selector: 'app-my-books',
  templateUrl: './my-books.page.html',
  styleUrls: ['./my-books.page.scss'],
})
export class MyBooksPage implements OnInit {





  constructor(
    public data: DataService,
    public _auth: AuthService,

    public user_data: UsersDataService,
    public book_data: BookDataService,
    public _ts: ToolsetService,
    public modal: ModalController,
  ) {

    this.book_data.book_list.subscribe(data => {

      this.books = data
      this.is_loading = false
      this.filter_books()
      console.log('BOOK LIST UPDATED : ', this.books);

    })
 

    this._auth._user.subscribe(user_doc => {
      
      this.meta.current_user = user_doc
      this.is_loading = false 
      this.filter_books() 
      
    })

    this._auth.update_user_data()

    
    
  }

  ngOnInit() {  

  }


  is_loading = true
  books
  books_list

  meta: any = {
    search: '',
    current_user: {},
    user_list: []
  }


  ionViewWillEnter() {

    this.load_data() 

  }

  update_user_data() {
    return this._auth.update_user_data()
  }
 

  has_owned(book) {

    const current_user = this.meta.current_user

    if (current_user && current_user['my_books'] && current_user['my_books']) {

      if (current_user['my_books'][book['_id']]) {
        return true
      }

    }

    return false

  }


  checkout_book(book) {
 
    const current_user = this.meta.current_user 

    this._ts.show_alert({
      title: 'Checkout Book?',
      html: 'Are you sure to checkout Book?',
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
    }).then(result => {

      if (result.value) {
        this.book_data.checkout_book(book['_id'], current_user['_id']).then(data => {
 
          this.update_user_data()
          
          this._ts.show_alert({
            title: 'Checked-out Book!',
            html: `Book <b>${book['name']}</b> successfully checked-out! <br> - `,
            icon: 'success',
            showConfirmButton: false,
            timer: 2500
          })

        })
      }

    })

  }


  return_book(book) {

    const current_user = this.meta.current_user
  
    this._ts.show_alert({
      title: 'Return Book?',
      html: `Are you sure to return Book? <br><br> <b> ${book['name']} </b> <br> -`,
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
    }).then(result => {

      if (result.value) {
        this.book_data.return_book(book['_id'], current_user['_id']).then(data => {
 
          this.update_user_data()
  
          this._ts.show_alert({
            title: 'Book Returned!',
            html: `Book <b>${book['name']}</b> successfully returned! <br> - `,
            icon: 'success',
            showConfirmButton: false,
            timer: 2500
          })

        })
      }

    })

  }




  filter_books() {
    this.books_list = this.books.filter(b => {
      return (
        (this.meta && this.meta.current_user && this.meta.current_user['my_books'] && this.meta.current_user['my_books'][b['_id']] == true )
        &&
        (
          String(b.name).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, '').includes(String(this.meta.search).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, ''))
          || String(b.genre_cat).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, '').includes(String(this.meta.search).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, ''))
          || String(b.author).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, '').includes(String(this.meta.search).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, ''))
          || String(b._id).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, '').includes(String(this.meta.search).toLowerCase().replace('-', '').replace(/[^\w\s]/gi, ''))
        )
      )
    })
  }

  load_data() {

    this.is_loading = true
    return this.book_data.fetch_books().finally(() => {
      this.is_loading = false
    }) 

  }





  show_book_info(book) {

    this._ts.show_alert({
      title: 'Book Info',
      icon: 'info',
      html: `
        <ul class="list-group text-left">
          <li class="list-group-item list-group-item-action">
            Name: <span class="float-right text-primary font-weight-bold"> ${book['name'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Book ID: <span class="float-right"> ${book['_id'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Author: <span class="float-right"> ${book['author'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Genre/Category: <span class="float-right"> ${book['genre_cat'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Publish Date : <span class="float-right"> ${ this._ts.formatDate(book['pub_date'], 'dd-mmm-yyyy') || ''} </span>
          </li>
          
          <li class="list-group-item list-group-item-action">
            Stock : <span class="float-right position-absolute px-3 py-1 rounded text-light bg-danger font-weight-bold" style="font-size: 16px;right: 1rem;top: 0.45rem;"> 5 </span>
          </li>

          <li class="list-group-item list-group-item-action">
            Out : <span class="float-right position-absolute px-3 py-1 rounded text-light bg-danger font-weight-bold" style="font-size: 16px;right: 1rem;top: 0.45rem;"> 5 </span>
          </li>
           
        </ul>
      `,
      showConfirmButton: true
    })

  }





  delete_book(book) {

    if (book && book.reserved) {
      return this._ts.show_alert({
        title: 'Sorry!',
        icon: 'error',
        html: ` 
          <br>
          <b class="text-danger"> " ${book['name'] || ''} " </b>  
          <br> 
          <br> 
          is a <b class="text-success">Reserved Book</b>
          <br>You cannot delete a reserved book!
          <br>
          -
        `,
        timer: 4000,
        showConfirmButton: false,
      }).then(result => {
        return
      })


    }

    this._ts.show_alert({
      title: 'Delete Book ?',
      icon: 'warning',
      html: `
        <br>
        You're about to delete book <br> <b class="text-danger"> " ${book['name'] || ''} " </b>  <br> Are you sure? 
        <br>
        <br>

        <ul class="list-group text-left">
          <li class="list-group-item list-group-item-action">
            Name: <span class="float-right text-primary font-weight-bold"> ${book['name'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Book ID: <span class="float-right"> ${book['_id'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Author: <span class="float-right"> ${book['author'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Genre/Category: <span class="float-right"> ${book['genre_cat'] || ''} </span>
          </li>
          <li class="list-group-item list-group-item-action">
            Publish Date : <span class="float-right"> ${this._ts.formatDate(book['pub_date'], 'dd-mmm-yyyy') || ''} </span>
          </li>
          
          <li class="list-group-item list-group-item-action">
            Stock : <span class="float-right position-absolute px-3 py-1 rounded text-light bg-danger font-weight-bold" style="font-size: 16px;right: 1rem;top: 0.45rem;"> 5 </span>
          </li>

          <li class="list-group-item list-group-item-action">
            Out : <span class="float-right position-absolute px-3 py-1 rounded text-light bg-danger font-weight-bold" style="font-size: 16px;right: 1rem;top: 0.45rem;"> 5 </span>
          </li>

        </ul>
      `,
      showConfirmButton: true,
      confirmButtonText: 'Yes',
      showCancelButton: true,
    }).then(result => {
      if (result.value) {

        this.book_data.delete_book(book).then(data => {

          this.book_data.fetch_books()

          this._ts.show_alert({
            title: 'Deleted!',
            html: `<br> User <b class="text-danger">" ${book['name'] || ''} "</b> <br> with Book ID: <b> ${book['_id'] || ''} </b> <br> has been deleted successfully!`,
            icon: 'success',
            showConfirmButton: false,
            timer: 2500
          })

        })

      }
    })

  }


  open_book_form(mode = 'ADD', book = {}) {

    console.log('Opening ' + mode + ' book: ', book);

    // this.modal.create({
    //   component: BookFormComponent,
    //   backdropDismiss: false,
    //   componentProps: {
    //     book: book,
    //     meta: { title: mode + ' Book', mode }
    //     // lastName: 'Adams',
    //     // middleInitial: 'N'
    //   }
    // }).then(modal => {
    //   modal.present()
    // })


  }

 
  open_book_comments(book = {}) {

    this.modal.create({
      component: BookCommentComponent,
      backdropDismiss: false,
      componentProps: {
        book: book,
        meta: { title: 'Comments/Ratings', current_user: this.meta.current_user },
        comments: []
      }
    }).then(modal => {
      modal.present()
    })

  }



}
