import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookSearchPageRoutingModule } from './book-search-routing.module';

import { BookSearchPage } from './book-search.page';
import { BookCommentComponent } from '../books/book-comment/book-comment.component';

import { StarRatingModule } from 'ionic4-star-rating';
import { SharedPageModule } from '../common/modules/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookSearchPageRoutingModule,
  ],

  declarations: [
    BookSearchPage,
  ]
})
export class BookSearchPageModule { }
