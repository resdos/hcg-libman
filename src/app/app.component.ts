import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';
import { ToolsetService } from './common/services/toolset.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home',
      permissions: ["ADMIN", ]
    },
    // {
    //   title: 'List',
    //   url: '/list',
    //   icon: 'list'
    // },
    {
      title: 'Manage Books',
      url: '/books',
      icon: 'bookmarks',
      permissions: ["ADMIN", "LIBRARIAN", "READER", ]
    },
    {
      title: 'Manage Users',
      url: '/users',
      icon: 'ios-contact',
      permissions: ["ADMIN", ]
    },
    {
      title: 'Roles & Permissions',
      url: '/roles-permissions',
      icon: 'key',
      permissions: ["ADMIN" ]
    },
    {
      title: 'My Books',
      url: '/my-books',
      icon: 'book',
      permissions: ["ADMIN", "LIBRARIAN", "READER"]
    },
    // {
    //   title: 'Book Search',
    //   url: '/my-books',
    //   icon: 'filing'
    // },
    // {
    //   title: 'Settings',
    //   url: '/settings',
    //   icon: 'settings'
    // },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private _auth: AuthService,
    private _ts: ToolsetService,
    private router: Router,
  ) {
    this.initializeApp();


    this._auth._user.subscribe(user_doc => {

      this.meta.current_user = user_doc 

    })

  }


  logout() {
    this._auth.logout()  
    this.router.navigateByUrl('/login')
  }

  ionViewWillEnter() {

    let current_user = JSON.parse(localStorage.getItem('_cap_user') || '')
    current_user = current_user['user']
    // this.user = current_user
    
  }

  meta : any = {
    current_user: {}
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();




    });
  }
}
