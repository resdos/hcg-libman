import { Injectable } from '@angular/core';


// import PouchDB from 'pouchdb';

import PouchDB from 'pouchdb/dist/pouchdb';
import PouchFind from 'pouchdb/dist/pouchdb.find';





import { environment } from 'src/environments/environment';

import { ToolsetService } from './toolset.service';
import { resolve } from 'url';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { HttpClient } from '@angular/common/http';


// import * as PouchDB from 'pouchdb/dist/pouchdb'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // data
  constructor(
    public _ts: ToolsetService,
    public http: HttpClient,


  ) {

    // this.data  = new PouchDB(environment.couchdb_url, { skip_setup: true })


    // this.data.logIn('admin', 'admin').then(function (user) {
    //   console.log("I'm User.", user);
    // });




    // CREATE INDEX
    this.setup_data()



  }


  setup_index() {

    // // CREATE INDEX
    // this.get('_design/type_role_index').then((doc) => {

    //   console.log('type_role_index exists!');

    // }).catch((err) => {
    //   console.log(err);
    //   this.createIndex({
    //     index: {
    //       fields: ['type', 'role'],
    //     },
    //     name: 'type_role_index',
    //     ddoc: 'type_role_index',
    //     type: 'json',
    //   })
    // });





    // this.get('_design/type_index').then((doc) => {

    //   console.log('type_index exists!');

    // }).catch((err) => {
    //   console.log(err);

    //   this.createIndex({
    //     index: {
    //       fields: ['type'],
    //     },
    //     name: 'type_index',
    //     ddoc: 'type_index',
    //     type: 'json',
    //   })
    // });





    // this.get('_design/role_index').then((doc) => {

    //   console.log('role_index exists!');

    // }).catch((err) => {
    //   console.log(err);

    //   this.createIndex({
    //     index: {
    //       fields: ['role'],
    //     },
    //     name: 'role_index',
    //     ddoc: 'role_index',
    //     type: 'json',
    //   })
    // });
  }


  setup_data() {


    this.setup_index()




    // MASTER DATA
    this.get('ROLES_PERMISSIONS').then((doc) => {
      // handle doc
      console.log('ROLES_PERMISSIONS exists!');

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "ROLES_PERMISSIONS",
        "type": "meta_data",
        "roles": [
          "ADMIN",
          "LIBRARIAN",
          "READER"
        ],
        "role_permissions": {
          "ADMIN": [
            "M_ACCS_BK_SRCH",
            "M_ACCS_HOME",
            "M_ACCS_MENU",
            "M_ACCS_MYBOOKS",
            "M_ACCS_USERS",
            "M_ACCS_ROLES_PERMS",
            "M_ACCS_BOOKS",
            "SHOW_BOOK",
            "SEARCH_BOOK",
            "COMMENT_BOOK",
            "RETURN_BOOK",
            "CHECKOUT_BOOK",
            "ASSIGN_BOOK",
            "ADD_BOOK",
            "EDIT_BOOK",
            "DELETE_BOOK",
            "SHOW_USER",
            "ADD_USER",
            "EDIT_USER",
            "DELETE_USER",
            "SHOW_USER_ROLE",
            "ADD_USER_ROLE",
            "EDIT_USER_ROLE",
            "DELETE_USER_ROLE"
          ],
          "LIBRARIAN": [
            "M_ACCS_BK_SRCH",
            "M_ACCS_HOME",
            "M_ACCS_MENU",
            "M_ACCS_MYBOOKS",
            "M_ACCS_BOOKS",
            "SHOW_BOOK",
            "SEARCH_BOOK",
            "COMMENT_BOOK",
            "RETURN_BOOK",
            "CHECKOUT_BOOK",
            "ASSIGN_BOOK",
            "ADD_BOOK",
            "EDIT_BOOK",
            "DELETE_BOOK"
          ],
          "READER": [
            "M_ACCS_BK_SRCH",
            "M_ACCS_HOME",
            "M_ACCS_MENU",
            "M_ACCS_MYBOOKS",
            "SHOW_BOOK",
            "SEARCH_BOOK",
            "COMMENT_BOOK",
            "RETURN_BOOK",
            "CHECKOUT_BOOK"
          ]
        },
        "permissions": [
          "M_ACCS_BK_SRCH",
          "M_ACCS_HOME",
          "M_ACCS_MENU",
          "M_ACCS_MYBOOKS",
          "M_ACCS_USERS",
          "M_ACCS_ROLES_PERMS",
          "M_ACCS_BOOKS",
          "SHOW_BOOK",
          "SEARCH_BOOK",
          "COMMENT_BOOK",
          "RETURN_BOOK",
          "CHECKOUT_BOOK",
          "ASSIGN_BOOK",
          "ADD_BOOK",
          "EDIT_BOOK",
          "DELETE_BOOK",
          "SHOW_USER",
          "ADD_USER",
          "EDIT_USER",
          "DELETE_USER",
          "SHOW_USER_ROLE",
          "ADD_USER_ROLE",
          "EDIT_USER_ROLE",
          "DELETE_USER_ROLE"
        ]
      })

    });



    this.get('CAT1-1234567801').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "CAT1-1234567801",
        "type": "BOOK",
        "book_index": "1234567801",
        "reserved": true,
        "name": "Book-1",
        "genre_cat": "CAT-1",
        "author": "Author-1",
        "pub_date": "2019-02-22"
      })

    });



    this.get('CAT1-1234567802').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "CAT1-1234567802",
        "type": "BOOK",
        "book_index": "1234567802",
        "reserved": true,
        "name": "Book-2",
        "genre_cat": "CAT-1",
        "author": "Author-1",
        "pub_date": "2019-02-22"
      })

    });



    this.get('CAT2-1234567803').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "CAT2-1234567803",
        "type": "BOOK",
        "book_index": "1234567803",
        "reserved": true,
        "name": "Book-3",
        "genre_cat": "CAT-2",
        "author": "Author-1",
        "pub_date": "2018-02-21"
      })

    });



    this.get('CAT2-1234567804').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "CAT2-1234567804",
        "type": "BOOK",
        "book_index": "1234567804",
        "reserved": true,
        "name": "Book-4",
        "genre_cat": "CAT-2",
        "author": "Author-2",
        "pub_date": "2019-02-22"
      })

    });



    this.get('CAT2-1234567805').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "CAT2-1234567805",
        "type": "BOOK",
        "book_index": "1234567805",
        "reserved": true,
        "name": "Book-5",
        "genre_cat": "CAT-2",
        "author": "Author-2",
        "pub_date": "2019-02-22"
      })

    });



    this.get('CAT3-1234567806').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "CAT3-1234567806",
        "type": "BOOK",
        "book_index": "1234567806",
        "reserved": true,
        "name": "Book-6",
        "genre_cat": "CAT-3",
        "author": "Author-2",
        "pub_date": "2019-02-22"
      })

    });










    //  MASTER USERS
    this.get('ahmed-00001').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "ahmed-00001",
        "type": "USER",
        "role": "ADMIN",
        "user_index": "00001",
        "reserved": true,
        "username": "ahmed",
        "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
        "first_name": "Ahmed",
        "last_name": "Sharif",
        "email": "ahmed.sh@gmail.com",
        "phone": "+97433866049"
      })

    });




    this.get('libr-00004').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "libr-00004",
        "type": "USER",
        "role": "LIBRARIAN",
        "user_index": "00004",
        "reserved": true,
        "username": "libr",
        "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
        "first_name": "Librarian",
        "last_name": "User",
        "email": "libr@gmail.com",
        "phone": "+97433866049"
      })

    });




    this.get('peter-00002').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "peter-00002",
        "type": "USER",
        "role": "READER",
        "user_index": "00002",
        "reserved": true,
        "username": "peter",
        "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
        "first_name": "Peter",
        "last_name": "Parker",
        "email": "peterpr@gmail.com",
        "phone": "+97433866049"
      })

    });




    this.get('user3-00003').then((doc) => {
      // handle doc 

    }).catch((err) => {
      console.log(err);

      this.put({
        "_id": "user3-00003",
        "type": "USER",
        "role": "READER",
        "user_index": "00003",
        "reserved": true,
        "username": "user3",
        "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
        "first_name": "User",
        "last_name": "3",
        "email": "user4@gmail.com",
        "phone": "+9743386604956"
      })

    });



  }



  login_user(user_id, password) {

    return new Promise((resolve, reject) => {

      this.find({
        selector: {
          type: "USER",
          "$or": [
            { "username": user_id },
            { "_id": user_id },

          ],
          password: this._ts.hash(password)
        },
        use_index: "type_role_index"
      }).then(result => {
        const is_valid = result && result.docs && result.docs.length
        if (is_valid && result.docs.length == 1) {
          const { password, ...user } = result.docs[0]
          user.name = user.first_name + " " + user.last_name

          this.get('ROLES_PERMISSIONS').then(result => {
            // permissions: [...this.rp_data.roles_permissions.value['role_permissions'][new_doc['role']]]

            const user_permissions = result.role_permissions[user.role]
            user.permissions = user_permissions

            user.has_permission = {}

            if (user && user.permissions && user.permissions.length) {

              for (let key of user.permissions) {
                user.has_permission[key] = true;
              }

            }
            

            resolve({ is_authenticated: true, user, msg: 'Login Success!' })

          })

        } else if (is_valid && result.docs.length > 1) {
          reject({ is_authenticated: false, msg: 'User Locked! Please contact Administrator!' })
        } else {
          reject({ is_authenticated: false, msg: 'Invalid Credential!' })
        }
      }).catch(err => {

        // this._ts.show_alert({
        //   title: 'Network Error',
        //   icon: 'error',
        //   html: `Can't connect to Server!`,
        //   timer: 2000
        // })
        reject({ is_authenticated: false, msg: 'Invalid Credential!', err })
      })

    })

  }




  remove(query, http_options = {}): Promise<any> {

    return new Promise((resolve, reject) => {

      if (query && query['rev'], (query._id || query.id)) {

        const post_data = { rev: (query['_rev'] || query['rev']) }
        const post_url = environment.couchdb_url + (query['_id'] || query['id']) + '/'

        this.http.delete(post_url, { ...http_options, params: { ...post_data } }).subscribe(data => resolve(data), err => {
          reject(err)
          this._ts.toast({
            message: 'Network Error!',
            duration: 2000,
          })

        })


      }

    })

  }

  delete(query, http_options = {}): Promise<any> {
    return this.remove(query, http_options)
  }

  get(query: Array<string> | string, http_options = {}): Promise<any> {

    // console.log('called..get : ', query);


    return new Promise((resolve, reject) => {

      if (typeof query == 'string') {

        const post_data = query
        const post_url = environment.couchdb_url + ''

        // console.log('posting..get: ', post_url + post_data);
        this.http.get(post_url + post_data, http_options).subscribe(data => resolve(data), err => {
          reject(err)
          this._ts.toast({
            message: 'Network Error!',
            duration: 2000,
          })

        })


      } else if (typeof query == 'object' && query.length) {

        const post_data = { docs: query.map(d => { return { id: d } }) }
        const post_url = environment.couchdb_url + '_bulk_get'

        this.http.post(post_url, post_data, http_options).subscribe(data => {

          if (data && data['results']) {
            data = data['results'].map(d => {

              return (d.length) ? [...d.docs] : d['docs']['ok']
            })
          }
          resolve(data)
        }, err => {
          reject(err)
          this._ts.toast({
            message: 'Network Error!',
            duration: 2000,
          })

        })

      }



    })

  }



  put(docs, http_options = {}): Promise<any> {

    return new Promise((resolve, reject) => {

      if (docs.length) { // Array of Objects
        const post_data = { docs }
        const post_url = environment.couchdb_url + '_bulk_docs'

        this.http.post(post_url, post_data, http_options).subscribe(data => resolve(data), err => {
          reject(err)
          this._ts.toast({
            message: 'Network Error!',
            duration: 2000,
          })

        })

      } else { // Single Objects
        const post_data = { ...docs }
        const post_url = environment.couchdb_url + ''

        this.http.post(post_url, post_data, http_options).subscribe(data => resolve(data), err => {
          reject(err)
          this._ts.toast({
            message: 'Network Error!',
            duration: 2000,
          })

        })
      }

    })

  }

  find(query, http_options = {}): Promise<any> {

    return new Promise((resolve, reject) => {

      if (query && query['selector']) {
        const post_data = { ...query }
        const post_url = environment.couchdb_url + '_find'

        this.http.post(post_url, post_data, http_options).subscribe(data => resolve(data), err => {
          reject(err)
          this._ts.toast({
            message: 'Network Error!',
            duration: 2000,
          })

        })

      }

    })

  }



  index(query, http_options = {}): Promise<any> {

    return new Promise((resolve, reject) => {

      if (query && query.index) {
        const post_data = { ...query }
        const post_url = environment.couchdb_url + '_index'

        this.http.post(post_url, post_data, http_options).subscribe(data => resolve(data), err => {
          reject(err)
          this._ts.toast({
            message: 'Network Error!',
            duration: 2000,
          })

        })

      }

    })

  }

  createIndex(query, http_options = {
    // "headers": {
    //   "content-type": "application/json", 
    //   "postman-token": "b6945c41-8f1d-66c4-4854-a345467abc54"
    // }
  }): Promise<any> {

    return new Promise((resolve, reject) => {

      if (query && query.index) {
        const post_data = { ...query }
        const post_url = environment.couchdb_url + '_index'

        this.http.post(post_url, post_data, http_options).subscribe(data => resolve(data), err => {
          reject(err)
          this._ts.toast({
            message: 'Network Error!',
            duration: 2000,
          })

        })

      }

    })

  }



}
