import { Injectable } from '@angular/core';

import dateformat from 'dateformat'
import Swal from 'sweetalert2'
import $ from 'jquery'
import { sha256, sha224 } from 'js-sha256';


import { LoadingController, ToastController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';


@Injectable({
  providedIn: 'root'
})
export class ToolsetService {

  constructor(
    private loadingCtrl: LoadingController,
    private toastController: ToastController,

  ) {

    
   }

 
  has_permission(perm) {
    //  * ngIf="user?.permissions?.includes('SHOW_USER')"

    console.log('CALLED FOR PERMISSION');
    
    let user = localStorage.getItem('_cap_user')
    user = (user && JSON.parse(user)) ? JSON.parse(user) : null
    if (user && user['user']) {
      user = user['user']
      if (user['permissions']) {
        return user['permissions'].includes(perm)
      }
    } 

    return false

  } 

  show_alert(options) {
    return Swal.fire(options)
  }

  toast_target
  toast(options) { 
    return new Promise((resolve, reject) => {

      this.toastController.create({
        message: 'Toast Alert',
        duration: 2000,
        ...options
      }).then(toast => {

        this.toast_target = toast

          this.toast_target.present()

          resolve(this.toast_target)
        })

    })

  }

  loader_target
  show_loader(options) {

    return new Promise((resolve, reject) => {
      
      this.loadingCtrl
        .create({
          message: 'Loading..',
          translucent: true,
          spinner: 'crescent',
          cssClass: 'loading-spinner',
          ...options
        }).then(load_el => {
          
          this.loader_target = load_el
  
          this.loader_target.present()

          resolve(this.loader_target)
        })

    })
    
  }


  hide_loader() {
    this.loader_target.dismiss()
  }

  remove_cache(tag: string) {
    localStorage.removeItem(tag)
  }


  set_cache(tag: string, data: any) {
    console.log('Caching Data : ', tag, " | ", data);
    const cache = JSON.stringify([data])
    localStorage.setItem(tag, cache)
    return this.get_cache(tag)
  }


  get_cache(tag: string, path: string = "") {
    // console.log('Accessing Cache : ', tag, ' | at : ', path);

    const data = JSON.parse(localStorage.getItem(tag))
    const cache = (data && data.length) ? data[0] : undefined
    return (cache) ? this.ob_path(path, cache) : undefined
  }








  formatDate(date: any, format: any = 'yyyy-mm-dd HH:MM:ss') {
    return dateformat(date, format)
  }

  ob_path(path, obj = self, separator = '.') {
    let properties = Array.isArray(path) ? path : path.split(separator)
    return properties.reduce((prev, curr) => prev && prev[curr], obj) || obj
  }

  obClone(ob) {
    return JSON.parse(JSON.stringify(ob))
  }

  compareObs(a, b) {
    return JSON.stringify(a) == JSON.stringify(b)
  }

  ob_includes(ob, arr) {
    const src = JSON.stringify(JSON.parse(JSON.stringify(arr)))
    const sub = JSON.stringify(JSON.parse(JSON.stringify(ob)))
    return src.includes(sub)
  }

  flatten(obj) {
    const array = Array.isArray(obj) ? obj : [obj];
    return array.reduce(function (acc, value) {
      acc.push(value);
      if (value.children) {
        acc = acc.concat(this.flatten(value.children));
        delete value.children;
      }
      return acc;
    }, []);
  }

  flat_dob(ob) {
    var toReturn = {};

    for (var i in ob) {
      if (!ob.hasOwnProperty(i)) continue;

      if ((typeof ob[i]) == 'object' && ob[i] !== null) {
        var flatObject = this.flat_dob(ob[i]);
        for (var x in flatObject) {
          if (!flatObject.hasOwnProperty(x)) continue;

          toReturn[i + '.' + x] = flatObject[x];
        }
      } else {
        toReturn[i] = ob[i];
      }
    }
    return toReturn;
  }
 


  slide_target(target, duration = 400, direction = 0) {
    switch (direction) {

      case 0: { $('.' + target).slideToggle(duration); break; }
      case 1: { $('.' + target).slideUp(duration); break; }
      case 2: { $('.' + target).slideDown(duration); break; }

    }
  }

  hash(data) {

    return sha256(String(data))

    return btoa(data)
    // Z = X + X1 + XN + XHN > YHS 
    let hash = btoa(data)
    hash = btoa(hash + hash[0] + hash[hash.length - 1] + (hash.substr(Math.floor((hash.length) / 2))).split('').reverse().join(''))
    hash = hash.substr(0, Math.abs((hash.length) / 2)) + hash.substr(Math.floor((hash.length) / 2))
    return hash
  }

  
  unhash(data) {
    return atob(data)
    // Z = X + X1 + XN + XHN > YHS 
    let hash = atob(data)
    hash = hash.substr(0, (hash.length - 2 - Math.floor((hash.length) / 3)))
    hash = atob(hash)
    return hash
  }  


}
