import { TestBed } from '@angular/core/testing';

import { ToolsetService } from './toolset.service';

describe('ToolsetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToolsetService = TestBed.get(ToolsetService);
    expect(service).toBeTruthy();
  });
});
