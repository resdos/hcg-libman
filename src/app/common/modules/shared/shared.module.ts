import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SharedPageRoutingModule } from './shared-routing.module';

import { SharedPage } from './shared.page';

import { StarRatingModule } from 'ionic4-star-rating';
import { BookFormComponent } from 'src/app/books/book-form/book-form.component';
import { BookCommentComponent } from 'src/app/books/book-comment/book-comment.component';
import { BookAssignComponent } from 'src/app/books/book-assign/book-assign.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedPageRoutingModule,

    StarRatingModule,

  ], 
  entryComponents: [BookFormComponent, BookCommentComponent, BookAssignComponent],
  declarations: [SharedPage, BookFormComponent, BookCommentComponent, BookAssignComponent],
})
export class SharedPageModule {}
