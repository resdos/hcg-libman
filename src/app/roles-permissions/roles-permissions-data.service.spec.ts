import { TestBed } from '@angular/core/testing';

import { RolesPermissionsDataService } from './roles-permissions-data.service';

describe('RolesPermissionsDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RolesPermissionsDataService = TestBed.get(RolesPermissionsDataService);
    expect(service).toBeTruthy();
  });
});
