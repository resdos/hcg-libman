import { Component, OnInit } from '@angular/core';
import { DataService } from '../common/services/data.service';
import { IonItemSliding, IonItem } from '@ionic/angular';

import Swal from 'sweetalert2'
import { ToolsetService } from '../common/services/toolset.service';
import { RolesPermissionsDataService } from './roles-permissions-data.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-roles-permissions',
  templateUrl: './roles-permissions.page.html',
  styleUrls: ['./roles-permissions.page.scss'],
})
export class RolesPermissionsPage implements OnInit {

  constructor(
    public rp_data: RolesPermissionsDataService,
    public _ts: ToolsetService,
    public _auth: AuthService,
  ) {

    this.rp_data.load_roles_permissions()

    this.rp_data.roles_permissions.subscribe(data => {

      this.roles_permissions_doc = data
      this.is_loading = false

      console.log('USER LIST UPDATED : ', this.roles_permissions_doc);

    })


    this._auth._user.subscribe(user_doc => {

      this.meta.current_user = user_doc
      this.is_loading = false 

    })
    this._auth.update_user_data()


  }

  is_loading = true


  meta: any = {
    current_user: {}
  }

  roles_permissions_doc

  form = {
    permission: {
      value: '',
      old_value: '',
      mode: 'ADD'
    },
    role: {
      value: '',
      old_value: '',
      mode: 'ADD',
      role_permissions: []
    },
  }


  ionViewWillEnter() {

    this.load_data()

  }



  load_data() {

    this.is_loading = true
    return this.rp_data.load_roles_permissions().finally(() => {

      this.is_loading = false

    })

  }

  ngOnInit() {

  }


  edit_form(form, item, slidingItem) {
    // console.log('EDITING item: ', item);

    slidingItem.close()

    if (form == 'role') {
      this.form.role = {
        value: item,
        old_value: item,
        mode: 'EDIT',
        role_permissions: [...this.roles_permissions_doc.role_permissions[item]]
      }
    } else {
      this.form.permission = {
        value: item,
        old_value: item,
        mode: 'EDIT',
      }
    }


  }


  delete_item(form, item, slidingItem) {

    slidingItem.close()

    this._ts.show_alert({
      title: 'DELETE ?',
      html: `Are you sure to delete ${form.charAt(0).toUpperCase() + form.slice(1).toLowerCase()} <br> <b> ${item} </b> <br>-`,
      icon: 'warning',
      showConfirmButton: true,
      showCancelButton: true,
    }).then(result => {
      if (result.value) {

        const old_doc = this.roles_permissions_doc

        const match_index = this.roles_permissions_doc[form + 's'].indexOf(item)
        this.roles_permissions_doc[form + 's'].splice(match_index, 1)
        delete this.roles_permissions_doc['role_permissions'][item]  


        const new_doc = this.roles_permissions_doc
        this.roles_permissions_doc = null
        this.is_loading = true

        this.rp_data.update_roles_permissions(new_doc).then(result => {
          this.load_data().then(data => {

            this._ts.show_alert({
              title: 'Deleted',
              html: `${form.charAt(0).toUpperCase() + form.slice(1).toLowerCase()} <b> ${item} </b> <br> has been deleted successfully! <br>-`,
              icon: 'success',
              timer: 2500,
              showConfirmButton: false,
              showCancelButton: false,
            })

          })
        }).catch(err => {
          this.roles_permissions_doc = old_doc
        })

      }
    })

  }

  reset_form(form) {
    if (form == 'role') {
      this.form.role = {
        value: '',
        old_value: '',
        mode: 'ADD',
        role_permissions: []
      }
    } else {
      this.form.permission = {
        value: '',
        old_value: '',
        mode: 'ADD',
      }
    }
  }

  formatInput(text) {
    return String(text).toUpperCase().replace('-', '_').replace(/[^\w\s]/gi, '')
  }


  save_form(form) {

    const old_doc = this.roles_permissions_doc

    if (this.form[form].mode == 'EDIT') {
      this.updated_doc(form)
    } else if (this.form[form].mode == 'ADD') {
      this.insert_doc(form)
    }


  }

  insert_doc(form) {
    const old_doc = this.roles_permissions_doc

    if (form == 'role') {
      this.roles_permissions_doc['roles'].unshift(this.form.role.value)
      this.roles_permissions_doc['role_permissions'][this.form.role.value] = [...this.form.role.role_permissions ]
    } else if (form == 'permission') {
      this.roles_permissions_doc['permissions'].unshift(this.form.permission.value) 
    }

    const new_doc = this.roles_permissions_doc
    this.roles_permissions_doc = null
    this.is_loading = true

    this.rp_data.update_roles_permissions(new_doc).then(result => {
      this.reset_form(form)
      this.load_data()
    }).catch(err => {
      this.roles_permissions_doc = old_doc
    })
  }

  updated_doc(form) {
    const old_doc = this.roles_permissions_doc

    if (form == 'role') {
      const match_index = this.roles_permissions_doc['roles'].indexOf(this.form.role.old_value)
      this.roles_permissions_doc['roles'].splice(match_index, 1, this.form.role.value)
      this.roles_permissions_doc['role_permissions'][this.form.role.value] = [...this.form.role.role_permissions ]

    } else if (form == 'permission') {
      const match_index = this.roles_permissions_doc['permissions'].indexOf(this.form.permission.old_value)
      this.roles_permissions_doc['permissions'].splice(match_index, 1, this.form.permission.value)
    }

    const new_doc = this.roles_permissions_doc
    this.roles_permissions_doc = null
    this.is_loading = true

    this.rp_data.update_roles_permissions(new_doc).then(result => {
      this.reset_form(form)
      this.load_data()
    }).catch(err => {
      this.roles_permissions_doc = old_doc
    })
  }



}
