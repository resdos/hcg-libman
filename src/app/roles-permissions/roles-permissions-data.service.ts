import { Injectable } from '@angular/core';
import { DataService } from '../common/services/data.service';
import { ToolsetService } from '../common/services/toolset.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RolesPermissionsDataService {

  constructor(
    public db: DataService,
    public _ts: ToolsetService
  ) {
    this.load_roles_permissions()
   }

  is_loading = new BehaviorSubject(true)

  roles_permissions = new BehaviorSubject(null)

  load_roles_permissions() {

    return new Promise((resolve, reject) => {

      this.db.get('ROLES_PERMISSIONS').then( (result) => {
        // handle doc
        // console.log('ROLES_PERMISSIONS fetched!', result);
        this.roles_permissions.next(result)
        resolve(result)

      }).catch( (err) => {

        console.log(err);
        // this._ts.show_alert({
        //   title: 'Network Error',
        //   icon: 'error',
        //   html: `Can't connect to Server!`,
        //   timer: 2000
        // })

        reject(err)

      })

    })

  }


  update_roles_permissions(new_data) {

    return new Promise((resolve, reject) => {
 

      this.db.put(new_data).then(function (result) {

        resolve(result)

      }).catch(function (err) {

        console.log(err);
        reject(err)

      })

    })

  }
}
