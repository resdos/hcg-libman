import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RolesPermissionsPage } from './roles-permissions.page';

describe('RolesPermissionsPage', () => {
  let component: RolesPermissionsPage;
  let fixture: ComponentFixture<RolesPermissionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesPermissionsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RolesPermissionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
