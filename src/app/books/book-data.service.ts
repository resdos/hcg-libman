import { Injectable } from '@angular/core';
import { DataService } from '../common/services/data.service';
import { ToolsetService } from '../common/services/toolset.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookDataService {

  constructor(
    public db: DataService,
    public _ts: ToolsetService
  ) {
    this.fetch_books()
  }

  is_loading = new BehaviorSubject(true)
 

  book_list = new BehaviorSubject([])

  fetch_books() {

    return new Promise((resolve, reject) => {

      this.db.find({
        selector: {
          type: "BOOK",
        },
        use_index: "type_index"
      }).then(result => {

        const is_valid = result && result.docs && result.docs.length
        // handle doc
        console.log('BOOK LIST fetched!', result);

        if (is_valid) {
          const book_list = result.docs.map(u => {
            // const { password, ...book_data } = u
            const { ...book_data } = u
            return { ...book_data  }
          })
          this.book_list.next([...book_list])
        }
        resolve(result)

      }).catch(function (err) {

        console.log(err);
        // this._ts.show_alert({
        //   title: 'Network Error',
        //   icon: 'error',
        //   html: `Can't connect to Server!`,
        //   timer: 2000
        // })

        reject(err)

      })

    })

  }







  get_comments(book_id){

    return new Promise((resolve, reject) => {

      this.db.find({
        selector: {
          type: "BOOK_COMMENT",
          book_id: book_id
        }
      }).then(com => {
          
        resolve(com)
        
      }).catch(err => {
        reject(err)
      })

    })

  }



  post_comment(comment_data){


    return new Promise((resolve, reject) => {

      this.db.put({ ...comment_data }).then(com => {

        resolve(com)

      }).catch(err => {
        reject(err)
      })

    })

  }




  checkout_book(book_id, current_user_id) {
  

    return this.assign_book(book_id, current_user_id) 
  
  }




  assign_book(book_id, user_id) : Promise<any> {

    return new Promise((resolve, reject) => {

      this.db.get(user_id).then(udoc => {
        
        const user_doc = { ...udoc }
 
        if (user_doc['my_books']) {
          user_doc['my_books'][book_id] = true
        } else {
          user_doc['my_books'] = { [book_id]:true }
        }
 
        
        this.db.put(user_doc).then(data => { 
                    
          resolve(data)

        })

        
      }).catch(err => {
        reject(err)
      })

    })

  }

  

  return_book(book_id, current_user_id) {
  

    return new Promise((resolve, reject) => {

      this.db.get(current_user_id).then(udoc => {
        
        const user_doc = { ...udoc }
 
        delete user_doc['my_books'][book_id]  
        
        this.db.put(user_doc).then(data => { 
          
          console.log('RETURNED BOOK ' + book_id + ' : ', data);
          
          resolve(data)

        })

        
      }).catch(err => {
        reject(err)
      })

    })
  
  }





  save_book(new_data) {

    return new Promise((resolve, reject) => {


      let { _rev, mode, show_manage, _id, ...new_doc } = new_data

      // ADD MODE
      if (!new_data['_id']) {

        this.db.find({
          selector: {
            type: "BOOK",
          },
          sort: [{ _id: "desc" }],
          limit: 1,
          fields: ["book_index", "_id"]
        }).then(result => {

          const is_valid = result && result.docs && result.docs.length

          // GOT LAST USER INDEX
          if (is_valid) {
            const next_book_index = String((+result['docs'][0]['book_index'] + 1)).padStart(10, '0')

 
            const post_data = {
              ...new_doc,
              _id: new_doc['genre_cat'] + '-' + next_book_index,
              book_index: next_book_index,
            }

            console.log('post_data: ', post_data);

            this.db.put(post_data).then(function (result) {

              resolve(result)

            }).catch(function (err) {

              console.log(err);
              reject(err)

            })

          }

        })


      }

      // EDIT MODE
      else {

        this.db.get(new_data['_id']).then(result => {


          if (new_data['_id'] == result['_id'] && new_data['_rev'] == result['_rev']) {

            console.log('CONFIRMED BOOK: ', result);

            new_doc = {
              ...result, ...new_doc,
            }

            console.log('CONFIRM BOOK UPDATED: ', new_doc);

            this.db.put({ ...new_doc }).then(function (result) {

              resolve(result)

            }).catch(function (err) {

              console.log(err);
              reject(err)

            })


          }


        })

      }


    })

  }


  delete_book(book_doc) {

    return new Promise((resolve, reject) => {

      this.db.remove(book_doc).then(data => {
        resolve(data)
      }).catch(err => {
        reject(err)
      })

    })
  }



}
