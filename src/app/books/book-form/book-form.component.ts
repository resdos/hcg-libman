import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToolsetService } from 'src/app/common/services/toolset.service';
import { BookDataService } from '../book-data.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss'],
})
export class BookFormComponent implements OnInit {

  constructor(
    public modal: ModalController,
    public _ts: ToolsetService,
    public book_data: BookDataService,
    public _auth: AuthService,
    // public rp_data: RolesPermissionsDataService,

  ) {

    this._auth._user.subscribe(user_doc => {

      this.meta.current_user = user_doc

      console.log('META CUR USER GOT', this.meta);
      
    })
  }
 
  @Input() book
  @Input() meta: any = {
    mode: 'ADD',
    title: 'ADD Book',
    current_user: {},

  }



  form = {
    "_id": "",
    "type": "BOOK",
    "book_index": "",
    "name": "",
    "genre_cat": "",
    "author": "",
    "pub_date": ""
  }



  user
  ngOnInit() {

    this.form = { ...this.form, ...this.book } 

    console.log('this.form : ', this.form);


    this._auth.check_auth().then(payload => {
      this.user = JSON.parse(payload.value)
      this.user = this.user['user']
      console.log('permission: ', this.user);
    })
    
  }



  close_modal() {
    this.modal.dismiss({
      'dismissed': true
    });
  }






  save_book() {
 
    const { book_index, ...new_doc } = this.form

    this.book_data.save_book({ ...new_doc, mode: this.meta.mode }).then(data => {

      this.book_data.fetch_books()

      this._ts.show_alert({
        title: (this.meta.mode == 'ADD') ? 'Book Created!' : 'Book Updated!',
        icon: 'success',
        html: `Book <br> <b class="text-info"> ${new_doc.name}</b> <br>has been ${(this.meta.mode == 'ADD') ? 'created' : 'updated'} successfully!`,
        showConfirmButton: false,
        timer: 2000,
      })
      this.close_modal()
    })

  }

}
