import { Component, OnInit, Input } from '@angular/core';
import { ToolsetService } from 'src/app/common/services/toolset.service';
import { AuthService } from 'src/app/auth/auth.service';
import { BookDataService } from '../book-data.service';
import { ModalController } from '@ionic/angular';
import { UsersDataService } from 'src/app/users/users-data.service';

@Component({
  selector: 'app-book-assign',
  templateUrl: './book-assign.component.html',
  styleUrls: ['./book-assign.component.scss'],
})
export class BookAssignComponent implements OnInit {

  constructor(
    public _ts: ToolsetService,
    public _auth: AuthService,
    public book_data: BookDataService,
    public user_data: UsersDataService,
    public modal: ModalController,


  ) {

    this._auth._user.subscribe(user_doc => {

      this.meta.current_user = user_doc

      console.log('META CUR USER GOT', this.meta);

    })

    this.user_data.user_list.subscribe(users => {

      this.meta.users = users

      this.filter_users()

    })

    this.user_data.fetch_users()
    this._auth.update_user_data()

  }

  @Input() book
  @Input() meta: any = {
    // mode: 'ADD',
    title: 'Book Comments/Ratings',
    current_user: {},
    users: [],
    user_list: [],

  }

  filter_users() {
    if (this.meta && this.meta.users && this.meta.users.length) {
      console.log('FILTERING USERS: ', this.meta.users);
      if (this.book && this.book['_id']) {
        console.log('this.book[id]: ', this.book['_id']);


        this.meta.user_list = this.meta.users.filter(u => {
          if (u.my_books) {

            return (
              (u.my_books[this.book['_id']] != true)

              &&
              (
                String(u._id).toLowerCase().includes(String(this.form.user_search).toLowerCase())
                || String(u.name).toLowerCase().includes(String(this.form.user_search).toLowerCase())
              )
            )

          }

          return (
            (
              String(u._id).toLowerCase().includes(String(this.form.user_search).toLowerCase())
              || String(u.name).toLowerCase().includes(String(this.form.user_search).toLowerCase())
            )
          )


        })

        if (this.meta.user_list.length == 1) {
          this.form.user_id = this.meta.user_list[0]['_id']
        }

        if (!this.form.user_search) {
          this.form.user_id = ''
        }


      }


    }
  }

  select_user(user) {
    this.form.user_id = user['_id']
    this.form.user_search = user['name']
  }

  ngOnInit() {

    this.form = { ...this.form }

    this.form.book_id = this.book['_id']

    this.user_data.fetch_users()
    this.filter_users()



    console.log('this.form : ', this.form);
  }



  is_loading = false

  form: any = {
    book_id: "",
    user_id: "",
    user_search: '',
    // "reserved": false,
  }


  close_modal() {
    this.modal.dismiss({
      'dismissed': true
    });
  }


  assign_book() {
    this.is_loading = true

    const book = this.book
    const user = this.meta.users.find(u => u._id == this.form.user_id)

    this._ts.show_alert({
      title: 'Assign Book?',
      html: `
        You're about to assign 
        <br>
        <b> Book : <span class="text-danger"> ${book['name'] || ''} </span> </b> 
        <br>
        to user <b class="text-info"> ${ user['name'] || ''} </b> 
        <br> Are you sure?`,
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
    }).then(result => {

      if (result.value) {
        this.book_data.assign_book(this.form.book_id, this.form.user_id).then(data => {

          this._auth.update_user_data()
          this.form.user_search = ''
          this.user_data.fetch_users().finally(() => {
            this.filter_users()
          })


          this._ts.show_alert({
            title: 'Book Assigned!',
            html: `
              Book <b class="text-danger">${book['name']}</b> 
              Assigned to user <b class="text-info"> ${ user['name'] || ''} </b> 
              successfully! <br> - `,
            icon: 'success',
            showConfirmButton: false,
            timer: 2500
          })

        })
      }

      this.is_loading = false


    })


    // this.book_data.assign_book(this.form.book_id, this.form.user_id).then(data => {
    //   console.log('BOOK Assigned!', data);
    //   this._ts.show_alert({
    //     title: 'Book Assigned!',
    //     title: ``,
    //   })
    // })
  }

}
