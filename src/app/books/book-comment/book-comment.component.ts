import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { ModalController } from '@ionic/angular';
import { ToolsetService } from 'src/app/common/services/toolset.service';
import { BookDataService } from '../book-data.service';

@Component({
  selector: 'app-book-comment',
  templateUrl: './book-comment.component.html',
  styleUrls: ['./book-comment.component.scss'],
})
export class BookCommentComponent implements OnInit {

  constructor(
    public _ts: ToolsetService,
    public _auth: AuthService,
    public book_data: BookDataService,
    public modal: ModalController,


  ) {

    this._auth._user.subscribe(user_doc => {

      this.meta.current_user = user_doc

      console.log('META CUR USER GOT', this.meta);

    })
  }

  @Input() book
  @Input() comments
  @Input() meta: any = {
    // mode: 'ADD',
    title: 'Book Comments/Ratings',
    current_user: {},


  }

  form: any = {
    comment: '',
    rating: null,
    type: "BOOK_COMMENT",
    commented_at: "",
    comment_by: "",
    book_id: "",
    // "reserved": false,
  }



  ngOnInit() {  
  }

  ionViewWillEnter() {

    this.refresh_comments()
  }


  close_modal() {
    this.modal.dismiss({
      'dismissed': true
    });
  }




  reset_form() {
    this.form = {
      comment: '',
      rating: null,
      type: "BOOK_COMMENT",
      commented_at: "",
      comment_by: "",
      book_id: "",
      // "reserved": false,
    }
  }

  refresh_comments() {

    this.book_data.get_comments(this.book['_id']).then(comments => {
      console.log('GOT COMMENTS: ', comments);
      this.comments = comments['docs']
    })
  }

  save_book() {

    this.form = {
      ...this.form,
      commented_at: this._ts.formatDate(new Date(), 'yyyy-mm-dd HH:MM:ss'),
      comment_by_id: this.meta.current_user['_id'],
      comment_by: this.meta.current_user['name'],
      book_id: this.book['_id'],
      book: this.book['name'],
    }

    console.log('FORM: ', this.form);


    this.book_data.post_comment({ ...this.form }).then(data => {

      this.reset_form()
      this.refresh_comments()
      // this.close_modal()
    })

  }


}
