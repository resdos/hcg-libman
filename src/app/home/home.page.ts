import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private _auth: AuthService,

  ) { 


    this._auth._user.subscribe(user_doc => {

      this.meta.current_user = user_doc

    })
    this._auth.update_user_data()
    
  }

  meta :any ={
    current_user: {}
  }

  user
  ionViewWillEnter() {
    // console.log('Entering...', this._auth.isAuthenticated);

    this._auth.check_auth().then(payload => {
      console.log('has auth', payload.value);

      
      if (!payload.value) {
        window.location.pathname += ''
      } else {
        this.user = JSON.parse(payload.value)
        this.user = this.user['user']
      }

      console.log('has auth', this.user);

      

    }).catch(e => {
      window.location.pathname += ''
    })
  }

}
