# README # 

## Quick Steps
    - $  install ( CouchDB 2.1 ) 
    - $  Configure CouchDB with credential 
            - user: `admin`
            - pass: `admin`
    - $  `Create database with name libman` 
    - $  `npm install` 
    - $  `ng serve`  

Will start the api on port `4200`. Make request on api endpoints at: http://localhost:4200/

## Usage
    - $  Login to Admin with credential 
            - user: ahmed-00001
            - pass: 123 